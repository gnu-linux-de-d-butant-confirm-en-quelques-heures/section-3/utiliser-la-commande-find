# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant d'effectuer des recherches sur votre système Linux.

Ainsi nous verrons la commande `find` avec différentes options:
- `-name`: spécifie le nom du fichier à rechercher
- `-type`: spécifie le type de fichier à rechercher (répertoire, fichiers, etc...)
- `-user`: spécifie l'utilisateur auquel appartient les fichiers à rechercher
- `-empty`: ne recherche que les fichiers ou les répertoires vides

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/find:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

## Quelques informations avant de commencer

Dans ce premier exercice, nous allons essayer de comprendre comment utiliser la commande `find` de manière simple.

La commande `find` nous permet de faire des recherches sur notre système. On peut ainsi faire des recherches très spécifiques en ciblant les noms, les extensions, les types de fichiers, etc...

Par exemple, si on veut rechercher tous les fichiers appelés `passwd` qui sont situés à l'intérieur de notre répertoire `/etc`, on peut utiliser la commande `find /etc -name passwd`

De même, si on ne souhaite cibler que des fichiers ou des répertoires, on peut utiliser l'option `-type d` pour les répertoires, et `-type f` pour les fichiers. 

Par exemple pour rechercher uniquement les **dossiers** situés dans le répertoire **/var/log**, on peut utiliser la commande : `find /var/log -type d`

## A vous de jouer

On souhaite maintenant rechercher un fichier qui s'appelle **arabica.cafe**. On sait uniquement qu'il se trouve quelque part dans le répertoire `/tmp/exercices`...

**Q1: En utilisant la commande find, retrouvez le chemin complet du fichier "arabica.cafe" dans le répertoire /tmp/exercices**

<details><summary> Montrer la solution </summary>
<p>
find /tmp/exercices -name arabica.cafe
</p>
<p>
/tmp/exercices/cafes/colombie/arabica.cafe
</p>
</details>

On souhaite maintenant rechercher tous les **dossiers** qui se trouvent à l'intérieur du répertoire **/tmp/exercices**

**Q2: Cochez les dossiers que vous voyez apparaître dans votre recherche:**
- [ ] /tmp/exercices/televisions
- [ ] /tmp/exercices/cuisines
- [ ] /tmp/exercices/ordinateurs
- [ ] /tmp/exercices/cafes/pays
- [ ] /tmp/exercices/GNU/linux
- [ ] /tmp/exercices/appartements

<details><summary> Montrer la solution </summary>
<p>
find /tmp/exercices/ -type d
</p>
<p>
On retrouve les dossiers : /tmp/exercices/televisions, /tmp/exercices/cuisines, /tmp/exercices/GNU/linux, /tmp/exercices/appartements.
</p>
</details>

On souhaite maintenant rechercher tous les **fichiers** qui se trouvent à l'intérieur du répertoire **/tmp/exercices/cafes**

**Q3: Cochez les fichiers que vous voyez apparaître dans votre recherche:**
- [ ] /tmp/exercices/cafes/colombie/arabica.cafe
- [ ] /tmp/exercices/cafes/perou/carajillo.cafe
- [ ] /tmp/exercices/cafes/colombie/robusta.cafe
- [ ] /tmp/exercices/cafes/perou
- [ ] /tmp/exercices/cafes/redeye

<details><summary> Montrer la solution </summary>
<p>
find /tmp/exercices/cafes -type f
</p>
<p>
On retrouve les fichiers suivants : /tmp/exercices/cafes/colombie/arabica.cafe, et /tmp/exercices/cafes/colombie/robusta.cafe
</p>
</details>

<br/>

# Exercice 2

## Quelques informations avant de commencer

Dans ce deuxième exercice, nous allons essayer de comprendre comment utiliser la commande `find` de manière à trouver les fichiers et dossiers appartenant à un utilisateur particulier.

L'option `-user` permet de cibler, dans notre recherche, les fichiers et dossiers appartenant à un utilisateur en particulier.

Par exemple, si on veut rechercher tous les **fichiers** (`-type f`) situés dans le répertoire `/tmp/exercices/livres` qui appartiennent à l'utilisateur **root** (`-user root`), on peut utiliser la commande: 
`find /tmp/exercices/livres/ -user root -type f`

L'option `-empty` permet de cibler, dans notre recherche, les fichiers et dossiers qui sont vides.

Par exemple, si on veut rechercher tous les **dossiers** (`-type d`) situés dans le répertoire `/etc/network/` qui sont vides (`-empty`), on peut utiliser la commande : 
`find /etc/network -type d -empty`

## A vous de jouer

On souhaite maintenant rechercher tous les fichiers du répertoire `/tmp/exercices` appartenant à l'utilisateur **pedro**.

**Q1: Quel est le nom complet du fichier appartenant à l'utilisateur pedro ?**

<details><summary> Montrer la solution </summary>
<p>
find /tmp/exercices -user pedro -type f
</p>
<p>
/tmp/exercices/cafes/colombie/robusta.cafe 
</p>
</details>

On souhaite maintenant rechercher tous les fichiers du répertoire `/tmp/exercices` appartenant à l'utilisateur **robert**.

**Q2: Quel est le nom complet du fichier appartenant à l'utilisateur robert ?**

<details><summary> Montrer la solution </summary>
<p>
find /tmp/exercices -user robert -type f
</p>
<p>
/tmp/exercices/televisions/samsung
</p>
</details>

On souhaite maintenant rechercher tous les **dossiers** du répertoire `/tmp/exercices` qui sont vides.

**Q3: Sélectionnez les dossiers vides trouvés dans votre recherche ?**
- [ ] /tmp/exercices/pays
- [ ] /tmp/exercices
- [ ] /tmp/exercices/cafes/perou
- [ ] /tmp/exercices/ventilateurs
- [ ] /tmp/exercices/cuisines
- [ ] /tmp/exercices/appartements

<details><summary> Montrer la solution </summary>
<p>
find /tmp/exercices/ -type d -empty
</p>
<p>
On retrouve alors les dossiers suivants : /tmp/exercices/pays, /tmp/exercices/cafes/perou et /tmp/exercices/ventilateurs.
</p>
</details>
<br/>

# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

